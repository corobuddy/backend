from django.core.management.base import BaseCommand, CommandError
from challenges.models import UserProfile


class Command(BaseCommand):
    help = "Distributes new challenges to all users that requested so."

    def handle(self, *args, **options):
        for profile in UserProfile.objects.all():
            profile.find_challenge()
