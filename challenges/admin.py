from django.contrib import admin
from django.utils.translation import gettext_lazy as _
from challenges.models import (
    Badge,
    BadgeAward,
    Category,
    Challenge,
    ChallengeLink,
    ChallengeMatch,
    UserProfile,
)

admin.AdminSite.site_header = _("Corobuddy Admin")
admin.AdminSite.site_title = admin.AdminSite.site_header


class BadgeAwardInlineAdmin(admin.TabularInline):
    model = BadgeAward


class ChallengeLinkInlineAdmin(admin.TabularInline):
    model = ChallengeLink


class ChallengeMatchInlineAdmin(admin.TabularInline):
    model = ChallengeMatch


@admin.register(Badge)
class BadgeAdmin(admin.ModelAdmin):
    list_display = ("name", "target_type", "target_value")
    inlines = [BadgeAwardInlineAdmin]


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    pass


@admin.register(Challenge)
class ChallengeAdmin(admin.ModelAdmin):
    list_display = ("title", "enabled", "is_timed", "start_timestamp")
    search_fields = ("title", "content")
    inlines = [ChallengeLinkInlineAdmin, ChallengeMatchInlineAdmin]


@admin.register(ChallengeMatch)
class ChallengeMatchAdmin(admin.ModelAdmin):
    list_display = ("user", "challenge", "valid_until", "finish_timestamp")


@admin.register(UserProfile)
class UserProfileAdmin(admin.ModelAdmin):
    pass
