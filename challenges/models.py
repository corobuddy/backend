import random
from datetime import timedelta
from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
from django.utils.translation import gettext_lazy as _


class UserProfile(models.Model):
    """Describes a user's profile, enriched with information obtained through the service they
    logged in with. This also stores their personal settings."""

    class Meta:
        verbose_name = _("User profile")
        verbose_name_plural = _("User profiles")

    def __str__(self):
        return str(self.user)

    user = models.OneToOneField(
        User,
        primary_key=True,
        on_delete=models.CASCADE,
        related_name="profile",
        verbose_name=_("Corresponding Django user"),
    )
    picture_url = models.URLField(
        max_length=300, null=True, blank=True, verbose_name=_("Profile picture URL"),
    )
    daily_challenge_target = models.PositiveSmallIntegerField(
        default=0,
        verbose_name=_("Daily personal challenge target"),
        help_text=_(
            "Maximum number of personal challenges this user wishes to receive every day."
        ),
    )

    def find_challenge(self, match_duration=timedelta(days=1)):
        """If applicable, find a new personal challenge that this user hasn't finished yet and
        create a new corresponding match.

        This method counts the number of active personal challenge matches. If that number is
        smaller than their target value, exactly new challenge is drawn (indipendent of the actual
        number of missing challenges).

        :param match_duration: The duration the resulting match object should be valid for, from
            now. This is ignored if no match is created.
        """
        personal_challenge_count = self.user.challenge_matches.filter(
            valid_from__lte=timezone.now(),
            valid_until__gte=timezone.now(),
            finish_timestamp__isnull=True,
        ).count()

        if personal_challenge_count >= self.daily_challenge_target:
            # No new challenge needs to be drawn.
            return

        # Find all candidates for challenges. Candidates are all personal challenges that don't have
        # match object corresponding to the user where the finished it (i.e. all challenges the user
        # hasn't completed yet).
        challenge_candidates = filter(
            lambda challenge: not challenge.matches.filter(
                user=self.user, finish_timestamp__isnull=False
            ).exists(),
            Challenge.objects.filter(is_timed=False, enabled=True),
        )
        challenge_candidates = list(challenge_candidates)

        if len(challenge_candidates) == 0:
            # No challenges are left to choose from, so the user gets none.
            return

        challenge = random.choice(challenge_candidates)
        challenge.matches.create(
            user=self.user, valid_until=timezone.now() + match_duration
        )


class Challenge(models.Model):
    """A challenge is a task for a user or a group of users."""

    class Meta:
        verbose_name = _("Challenge")
        verbose_name_plural = _("Challenges")

        _TIMED_CHALLENGE_HAS_TIMES = models.Q(is_timed__exact=False) | models.Q(
            start_timestamp__isnull=False, end_timestamp__isnull=False
        )
        constraints = [
            models.CheckConstraint(
                check=_TIMED_CHALLENGE_HAS_TIMES, name="timed_challenge_has_times",
            )
        ]

    def __str__(self):
        return self.title

    title = models.CharField(
        max_length=100,
        verbose_name=_("Title"),
        help_text=_("A prominent eye-caching text for this challenge."),
    )
    content = models.TextField(
        verbose_name=_("Content"),
        help_text=_("The main objective of the challenge that the user should solve.",),
    )
    prerequisites = models.TextField(
        null=True,
        blank=True,
        verbose_name=_("Prerequisites"),
        help_text=_("Tools or other things needed to complete this challenge."),
    )
    category = models.ForeignKey(
        "Category",
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        verbose_name=_("Category"),
    )
    estimated_time = models.IntegerField(
        null=True,
        blank=True,
        verbose_name=_("Time estimation (in minutes)"),
        help_text=_("Number of minutes this challenge will probably take."),
    )
    enabled = models.BooleanField(
        verbose_name=_("Enabled"),
        help_text=_(
            "Whether the challenge is enabled. This field takes precedence over the start and end times, setting this to false will completly hide the challenge.",
        ),
    )

    TYPE_CHOICES = [
        (False, _("Personal challenge")),
        (True, _("Community challenge")),
    ]
    is_timed = models.BooleanField(
        choices=TYPE_CHOICES,
        default=False,
        verbose_name=_("Challenge type"),
        help_text=_(
            "Community challenges appear during the given time frame and are the same for every player. Personal challenges are randomly distributed and every player gets their own set.",
        ),
    )
    start_timestamp = models.DateTimeField(
        null=True,
        blank=True,
        verbose_name=_("Starting time"),
        help_text=_(
            "Starting date and time for community challenges. Then it is first available and can be played.",
        ),
    )
    end_timestamp = models.DateTimeField(
        null=True,
        blank=True,
        verbose_name=_("Ending time"),
        help_text=_(
            "End date and time for community challenges. This must be after the starting time.",
        ),
    )

    def user_finish_timestamp(self, user: User):
        """Return the timestamp when a given user finished this challenge, or None."""
        qs = self.matches.filter(user=user, finish_timestamp__isnull=False)
        if qs.exists():
            return qs.latest("finish_timestamp").finish_timestamp
        else:
            return None


class ChallengeLink(models.Model):
    """An optional link that can be added to a challenge.

    Links may point to outside sources like YouTube videos, Instagram stories, etc.
    """

    class Meta:
        verbose_name = _("Additional challenge link")
        verbose_name_plural = _("Additional challenge links")

    challenge = models.ForeignKey(
        "Challenge",
        on_delete=models.CASCADE,
        related_name="links",
        related_query_name="link",
        verbose_name=_("Corresponding Challenge"),
    )
    target = models.URLField(max_length=200, verbose_name=_("Target URL"))
    title = models.CharField(max_length=100, verbose_name=_("Link title"))


class Category(models.Model):
    """Taxonomy that holds challenges of a shared theme."""

    class Meta:
        verbose_name = _("Category")
        verbose_name_plural = _("Categories")

    def __str__(self):
        return self.name

    name = models.CharField(max_length=100, verbose_name=_("Name"))


class ChallengeMatch(models.Model):
    """Created after a user accepts a challenge.

    This object maps a user to a challenge - showing that they may now complete the corresponding
    challenge. These are created when the user requests a challenge.
    """

    class Meta:
        verbose_name = _("Challenge match")
        verbose_name_plural = _("Challenge matches")

    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name="challenge_matches",
        related_query_name="challenge_match",
        verbose_name=_("Corresponding User"),
    )
    challenge = models.ForeignKey(
        "Challenge",
        on_delete=models.CASCADE,
        related_name="matches",
        related_query_name="match",
        verbose_name=_("Corresponding Challenge"),
    )
    valid_from = models.DateTimeField(auto_now_add=True, verbose_name=_("Valid from"),)
    valid_until = models.DateTimeField(verbose_name=_("Valid until"))
    finish_timestamp = models.DateTimeField(
        null=True,
        blank=True,
        verbose_name=_("Finished time"),
        help_text=_(
            "Date and time the user finished the challenge. If this is null, they didn't finish the challenge.",
        ),
    )


class Badge(models.Model):
    """A type of badge that can be awarded to a user."""

    class Meta:
        verbose_name = _("Badge")
        verbose_name_plural = _("Badges")

    def __str__(self):
        return self.name

    name = models.CharField(max_length=100, unique=True, verbose_name=_("Name"))
    description = models.TextField(verbose_name=_("Description"))

    TOTAL_COMPLETIONS = 0
    DAY_STREAK = 1
    TYPE_CHOICES = [
        (TOTAL_COMPLETIONS, _("Total challenge completions"),),
        (DAY_STREAK, _("Consecutive completions of a number of days"),),
    ]
    target_type = models.PositiveSmallIntegerField(
        choices=TYPE_CHOICES,
        verbose_name=_("Type of the target"),
        help_text=_(
            "What type of target the user must acheive in order to be awarded this badge.",
        ),
    )
    target_value = models.IntegerField(
        verbose_name=_("Value of the target"),
        help_text=_(
            "What exactly needs to be done in order to acheive this badge. The exact meaning of this field depends on the target type.",
        ),
    )
    target_category = models.ForeignKey(
        "Category",
        on_delete=models.CASCADE,
        null=True,
        blank=True,
        verbose_name=_("Target category limitation"),
        help_text=_(
            'Limit this badge to challenges in a specific category. Use this to create badges like "Complete 5 gardening challenges".',
        ),
    )

    def test_user_applicable(self, user: User):
        """Test whether a given user is currently applicable for this badge.

        :param user: The user to test.
        :return: True of False, depending on the state. A value of True may still be returned if the
            user already owns the badge.
        """
        if self.target_type == self.TOTAL_COMPLETIONS:
            # A user must have completed at least `target_value` different unique challenges in
            # order to receive the badge. Optionally this can be limited to challenges in a specific
            # category.
            qs = ChallengeMatch.objects.filter(
                user=user, finish_timestamp__isnull=False
            )
            if self.target_category:
                qs = qs.filter(challenge__category=self.target_category)
            user_completions = qs.values("challenge").count()
            return user_completions >= self.target_value
        elif self.target_type == self.DAY_STREAK:
            # todo testen
            qs = ChallengeMatch.objects.filter(
                user=user, finish_timestamp__isnull=False
            )
            qs = qs.filter(finish_timestamp__gte=timezone.now() - self.target_value)
            # auf catergories testen??

            distinct_dates = map(lambda match: match.finish_timestamp.date(), qs)
            distinct_dates = sorted(set(distinct_dates))

            highest_streak = 0
            prev = distinct_dates[0]
            for date in distinct_dates[1:]:
                if (date - prev).days == 1:
                    highest_streak += 1
                else:
                    highest_streak = 0
            return highest_streak >= self.target_value
        else:
            pass


class BadgeAward(models.Model):
    """Connects a user to a bagde as soon as the user is applicable for it."""

    class Meta:
        verbose_name = _("Badge award")
        verbose_name_plural = _("Badge awards")
        constraints = [
            models.UniqueConstraint(fields=["user", "badge"], name="unique_award")
        ]

    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name="received_awards",
        related_query_name="received_award",
        verbose_name=_("Corresponding user"),
    )
    badge = models.ForeignKey(
        "Badge",
        on_delete=models.CASCADE,
        related_name="distributed_awards",
        related_query_name="distributed_award",
        verbose_name=_("Corresponding badge"),
    )
    award_date = models.DateField(auto_now_add=True, verbose_name=_("Award date"))
