from django.contrib.auth.decorators import login_required
from django.http import (
    HttpResponseRedirect,
    HttpResponseNotFound,
    HttpResponseForbidden,
)
from django.shortcuts import render, redirect
from django.utils import timezone
from .forms import SettingsForm
from .models import Challenge, ChallengeMatch, UserProfile


def login(request):
    if request.user.is_authenticated:
        return redirect("home")
    return render(request, "login.html")


@login_required
def home(request):
    user_full_name = f"{request.user.first_name} {request.user.last_name}"
    try:
        user_profile = request.user.profile
    except UserProfile.DoesNotExist:
        user_profile = UserProfile.objects.create(user=request.user)
    user_picture = user_profile.picture_url
    completed_challenge_count = (
        ChallengeMatch.objects.filter(user=request.user, finish_timestamp__isnull=False)
        .values("challenge")
        .count()
    )

    challenges = []

    def challenge_repr(challenge):
        is_completed = True if challenge.user_finish_timestamp(request.user) else False
        return {
            "id": challenge.id,
            "title": challenge.title,
            "is_timed": challenge.is_timed,
            "is_completed": is_completed,
        }

    personal_challenge_matches = request.user.challenge_matches.filter(
        valid_from__lte=timezone.now(), valid_until__gte=timezone.now(),
    ).prefetch_related("challenge")
    for match in personal_challenge_matches:
        # For personal challenges, the end timestamp corresponds to the time when the match goes
        # invalid.
        challenges.append(
            {**challenge_repr(match.challenge), "end_timestamp": match.valid_until,}
        )

    community_challenges = Challenge.objects.filter(
        enabled=True,
        is_timed=True,
        start_timestamp__lte=timezone.now(),
        end_timestamp__gt=timezone.now(),
    )
    for challenge in community_challenges:
        challenges.append(
            {**challenge_repr(challenge), "end_timestamp": challenge.end_timestamp,}
        )

    return render(
        request,
        "home.html",
        context={
            "challenges": challenges,
            "user_full_name": user_full_name,
            "user_picture": user_picture,
            "completed_challenge_count": completed_challenge_count,
        },
    )


@login_required
def challenge_details(request, challenge_id):
    challenge = Challenge.objects.get(pk=challenge_id)
    finish_timestamp = challenge.user_finish_timestamp(request.user)
    links = list(challenge.links.all())

    return render(
        request,
        "challenge_details.html",
        context={
            "challenge": challenge,
            "links": links,
            "finish_timestamp": finish_timestamp,
        },
    )


@login_required
def finish_challenge(request, challenge_id):
    if not request.method == "POST":
        return HttpResponseNotFound()

    challenge = Challenge.objects.get(pk=challenge_id)

    def finish_challenge():
        match = ChallengeMatch.objects.get(
            user=request.user,
            challenge__pk=challenge_id,
            valid_from__lte=timezone.now(),
            valid_until__gt=timezone.now(),
        )
        match.finish_timestamp = timezone.now()
        match.save()

    if challenge.is_timed:
        if (
            challenge.start_timestamp > timezone.now()
            or challenge.end_timestamp < timezone.now()
        ):
            # If the challenge hasn't started yet or is already elapsed, you can't finish it.
            return HttpResponseForbidden()

        # For timed (community) challenges, a match object is created when the user finishes it.
        try:
            finish_challenge()
        except ChallengeMatch.DoesNotExist:
            challenge.matches.create(
                user=request.user,
                valid_until=timezone.now(),
                finish_timestamp=timezone.now(),
            )
    else:
        # For other challenges, the match object must alread be present when they would like to
        # finish it.
        try:
            finish_challenge()
        except ChallengeMatch.DoesNotExist:
            return HttpResponseForbidden()

    return HttpResponseRedirect(f"/challenge/{challenge_id}")


@login_required
def settings(request):
    if request.method == "POST":
        form = SettingsForm(request.POST)
        if form.is_valid():
            try:
                profile = request.user.profile
            except UserProfile.DoesNotExist:
                profile = UserProfile(user=request.user)

            profile.daily_challenge_target = form.cleaned_data["daily_challenge_target"]
            profile.save()

            return redirect("home")

    return render(request, "settings.html")
