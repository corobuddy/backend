from django import forms
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _


def validate_non_negative(value):
    if value < 0:
        raise ValidationError(
            _("%(value)s is a negative number"), params={"value": value},
        )


class SettingsForm(forms.Form):
    daily_challenge_target = forms.IntegerField(
        validators=[validate_non_negative],
        label=_("Daily personal challenge target"),
        help_text=_(
            "This is the maximum number of personal challenges you would like to receive every day. Settings this to zero means you will only get community challenges."
        ),
    )
