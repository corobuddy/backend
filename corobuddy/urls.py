"""corobuddy URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.urls import include, path
from challenges import views as challenges_views

urlpatterns = [
    path("", challenges_views.home, name="home"),
    path(
        "challenge/<int:challenge_id>",
        challenges_views.challenge_details,
        name="challenge_details",
    ),
    path(
        "challenge/<int:challenge_id>/finish",
        challenges_views.finish_challenge,
        name="finish_challenge",
    ),
    path("settings/", challenges_views.settings, name="settings"),
    path("login/", challenges_views.login, name="login"),
    path("logout/", auth_views.LogoutView.as_view(), name="logout"),
    path("social-auth/", include("social_django.urls", namespace="social")),
    path("admin/", admin.site.urls),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
