from .models import UserProfile


def get_picture_pipeline(
    backend, strategy, details, response, user=None, *args, **kwargs
):
    """Social auth pipline step that extracts the user's profile picture."""
    picture_url = None
    if backend.name == "facebook":
        picture_url = response["picture"]["data"]["url"]
    elif backend.name == "google-oauth2":
        picture_url = response["picture"]

    if picture_url:
        UserProfile(user=user, picture_url=picture_url).save()
