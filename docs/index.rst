.. Corobuddy documentation master file, created by
   sphinx-quickstart on Sun Mar 22 15:22:30 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Corobuddy's Dokumentation!
=====================================

CoroBuddy ist eine Anwendung zur Tagesstrukturierung in Zeiten von häuslicher (Selbst-) Quarantäne mit abwechslungsreichen, motivierenden und leicht umzusetzenden Individual- und Gruppen-Challenges.

Wir alle werden momentan Zeugen eines noch nie dagewesenen Phänomens: Fast ganz Deutschland sitzt in Selbst-Quarantäne fest. Zahllose Stunden alleine oder vor dem Bildschirm werden allerdings schnell zur Belastung. Wir ernähren uns ungesund, bewegen uns kaum und vereinsamen.

Wir brauchen Hilfe, Jemanden der uns Struktur gibt, uns Aufgaben stellt und uns jeden Tag daran erinnert, dass wir nicht alleine sind.

Corobuddy tut genau das. Es handelt sich um eine Web-Applikation, auf der sich Bürger*innen anmelden und dann alleine und gemeinsam Herausforderungen meistern und über sich hinauswachsen können.
Aus verschiedenen Kategorien fordert Corobuddy in regelmäßigen Abständen die Nutzenden zu Challenges heraus, von kreativen Aufgaben, über sportliche Aktivitäten bis hin zu wohltuender Selbstfürsorge. Die gleichbleibenden Zeiten geben uns die so wichtige Struktur zurück, die Aufgaben halten uns körperlich und geistig fit und natürlich sollen sie vor allem Spaß machen!
Abends folgt eine Communitychallenge, die für alle Nutzenden gleich aussieht. So wird zum Beispiel gleichzeitig aus dem Fenster gemeinsam ein Lied gesungen oder der Mehlbestand des Haushaltes wird mit hilfe von Tüchern in den Fenstern nach außen kommuniziert (vielleicht ein anderes Bsp?). So vergessen wir nicht, dass wir alle im selben Boot sitzen. Obwohl wir alle einzeln in unseren Wohnungen spielen, spielt niemand allein. In der weiteren Entwicklung soll außerdem eine Chat-Funktion hinzugefügt werden, um mittels GPS-Daten mit seinen Nachbarn kommunizieren zu können.

Wenn Corobuddy deutschlandweit genutzt wird, wird den Bürger*innen nicht nur psychisch und physisch dabei geholfen, diesen Ausnahmezustand zu überstehen. Vielleicht, ganz vielleicht, treten wir in ein paar Wochen wieder vor die Tür und sind auf einmal nicht mehr nur Nachbar*innen, sondern Freud*innen.


.. toctree::
   :maxdepth: 2
   :caption: Technische Dokumentation:

   modules/views.rst
   modules/models.rst

